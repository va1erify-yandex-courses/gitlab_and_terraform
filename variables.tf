# auth creds
variable "cloud_id" {
  default = "0"
}
variable "folder_id" {
  default = "0"
}
variable "token" {
  default = "0"
}

# network
variable "zone" {
  default = "ru-central1-a"
}

variable "cidr_v4" {
  default = "192.168.10.0/24"
}

# VMs
variable "platform" {
  default = "standard-v2"
}

variable "cpu" {
  default = 2
}
variable "core_fraction" {
  default = 5
}

variable "ram" {
  default = 0.5
}
variable "boot_disk_image_id" {
  # ubuntu 22.04 LTS
  default = "fd8ba9d5mfvlncknt2kd"
}
variable "boot_disk_size" {
  default = 8
}

variable "boot_disk_type" {
  default = "network-hdd"
}

variable "boot_disk_block_size" {
  default = 4096
}

variable "s3_bucket_name" {
  default = "va1erify-test-s3-bucket"
}




