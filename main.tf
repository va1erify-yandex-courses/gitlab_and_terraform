module "network" {
  source          = "./modules/network-create"
  name_network    = "main-network"
  name_subnetwork = "main-subnetwork"
  cidr_v4         = var.cidr_v4
}

module "nfs-server-vm" {
  source               = "./modules/vm-create"
  subnetwork_id        = module.network.subnetwork_id
  name                 = "nfs-server"
  ip_address           = "192.168.10.12"
  hostname             = "nfs-server"
  platform             = var.platform
  ram                  = var.ram
  cpu                  = var.cpu
  core_fraction        = var.core_fraction
  boot_disk_image_id   = var.boot_disk_image_id
  boot_disk_size       = var.boot_disk_size
  boot_disk_type       = var.boot_disk_type
  boot_disk_block_size = var.boot_disk_block_size
}

module "nfs-client-vm" {
  source               = "./modules/vm-create"
  subnetwork_id        = module.network.subnetwork_id
  name                 = "nfs-client"
  ip_address           = "192.168.10.13"
  hostname             = "nfs-client"
  platform             = var.platform
  ram                  = var.ram
  cpu                  = var.cpu
  core_fraction        = var.core_fraction
  boot_disk_image_id   = var.boot_disk_image_id
  boot_disk_size       = var.boot_disk_size
  boot_disk_type       = var.boot_disk_type
  boot_disk_block_size = var.boot_disk_block_size
}