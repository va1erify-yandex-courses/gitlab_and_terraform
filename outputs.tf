output "nfs_server_external_ip" {
  value = module.nfs-server-vm.external_ip
}

output "nfs_client_external_ip" {
  value = module.nfs-client-vm.external_ip
}