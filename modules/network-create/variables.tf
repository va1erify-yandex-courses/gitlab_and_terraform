variable "name_network" {}
variable "name_subnetwork" {}
variable "cidr_v4" {
  default = "10.10.10.0/24"
}
variable "zone" {
  default = "ru-central1-a"
}

