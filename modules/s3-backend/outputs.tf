output "s3-bucket-name" {
  value = var.s3_bucket_name
}

output "s3-access-key" {
  sensitive = true
  value = yandex_iam_service_account_static_access_key.s3_terraform_static_key.access_key
}

output "s3-secret-key" {
  sensitive = true
  value = yandex_iam_service_account_static_access_key.s3_terraform_static_key.secret_key
}