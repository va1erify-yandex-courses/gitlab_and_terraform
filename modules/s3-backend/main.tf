# Сервисный аккаунт для S3
resource "yandex_iam_service_account" "S3_SA" {
  name = "s3-terraform-backend"
}

# Добавление прав "editor" на folder
resource "yandex_resourcemanager_folder_iam_member" "s3_terraform_editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.S3_SA.id}"
}

# Статический ключ для сервисного аккаунта S3
#resource "yandex_iam_service_account_static_access_key" "s3_terraform_static_key" {
#  service_account_id = yandex_iam_service_account.S3_SA.id
#  description        = "static access key for object storage"
#}

# Бакет S3
resource "yandex_storage_bucket" "S3" {
#  access_key = yandex_iam_service_account_static_access_key.s3_terraform_static_key.access_key
#  secret_key = yandex_iam_service_account_static_access_key.s3_terraform_static_key.secret_key
  bucket = var.s3_bucket_name

  anonymous_access_flags {
    read = false
    list = false
    config_read = false
  }
}
